#ifndef S_TIMER
#define S_TIMER

#include <time.h>
#include "SUpdate.hpp"

using storm::SUpdate;

namespace storm
{
	class STimer: SUpdate
	{
	public:

		STimer();

		void			Update();
		void			start();
		void			stop();
		void			reset();
		bool			isRunning() {return running;};
		unsigned long	getTime();
		bool			isOver(unsigned long seconds);

	private:

		bool           resetted;
		bool           running;
		unsigned long  beg;
		unsigned long  end;
		unsigned long  passedTime;

	public:
		typedef void(*CallBack)();
		CallBack callBack;

		void setTimer(unsigned long milliseconds, CallBack cb);
	};
}

#endif