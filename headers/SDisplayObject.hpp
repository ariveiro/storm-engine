#ifndef _S_DISPLAY_OBJECT_
#define _S_DISPLAY_OBJECT_

#include <Iw2D.h>
#include "SUpdate.hpp"
#include "SInput.hpp"
#include "Shared_Ptr.hpp"

using storm::SUpdate;
using toolkit::Shared_Ptr;


namespace storm
{
	/**
	 * @brief Clase base para todos los objetos que vayan a ser
	 * dibujados en pantalla
	 */
	class SDisplayObject : public SUpdate
	{

	protected:

		CIwFMat2D		TransformMatrix;	// Matriz que contiene la informacion de rotacion, escala y traslacion
		CIwFVec2		Position;			// Posicion en X e Y del objeto

		float			Rotation;			// Rotacion del objeto
		float			Scale;				// Escala del objeto
		bool			Visible;			// Si un objeto sera renderizado o no
		bool			needRedraw;			// Dice es necesario reconstruir la matriz de transformacion
		bool			initialized;		// Dice si la matriz de transformación ha sido inicializado
		float			width;				// Ancho del objeto
		float			height;				// Alto del objeto
		float			pivotX;				// Punto de pivote en X
		float			pivotY;				// Punto de pivote en Y
		SDisplayObject *parent;				// Padre del objeto

	public:
		virtual float getWidth	() { return width;  };
		virtual float getHeight () { return height; };

	public:

		SDisplayObject();
		~SDisplayObject();

		void SetParent					(SDisplayObject *sprite) { parent = sprite; };
		void BuildTransformationMatrix	();
		bool CheckUserTouch				();

		virtual void Initialize	();
		virtual void Update		(){};
		virtual void Draw		(){};
		virtual void Release	(){};
		virtual void UpdatePos	() { needRedraw = true; }

	public:

		// COLLISION
		bool collides(float pos_x, float pos_y, float ewidth, float eheight);
		
		//	SETTERS
		//position
		virtual void	setPosition	(float x, float y);
		virtual void	setX		(float x);
		virtual void	setY		(float y);
		//rotation
		virtual void	setRotation	(float angle);
		//scale
		virtual void	setScale	(float scale);
		//visibility
		virtual void	setVisible	(bool show) { Visible = show; }
		//pivot
		virtual void	setPivot	(float x, float y);

		////////////

		// GETTERS
		//position
		virtual float const		getX() { return Position.x; };
		virtual float const		getY() { return Position.y; };

		//rotation
		virtual float const		getRotation() { return Rotation; };
		//scale
		virtual float const		getScale   () { return Scale; };
		//transform
		virtual CIwFMat2D		getTransform() { return TransformMatrix; };
		//visibility
		virtual bool			isVisible() const { return Visible; };
		///////////		
	};

	
}

#endif