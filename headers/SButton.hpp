#ifndef _S_BUTTON_
#define _S_BUTTON_

#include <Iw2D.h>
#include <string>
#include "SImage.hpp"
#include "SSprite.hpp"
#include "SInput.hpp"
#include <iostream>
#include "Shared_Ptr.hpp"
#include "SText.hpp"

using std::string;
using storm::SImage;
using storm::SSprite;
using storm::SInput;
using storm::SText;
using toolkit::Shared_Ptr;

namespace storm
{
	/**
	 * @brief Un boton simple compuesto por dos imagenes y un texto opcional.
	 * <p>Puedes pasar la misma textura dos veces. En ese caso, el boton tendra un efecto de escalado
	 * al ser clicado.</p>
	 * <p>Para reaccionar al boton presionado, es necesario pasar una funcion por referencia
	 * que sera lanzada cuando el boton sea pulsado.</p>
	 */
	class SButton : public SSprite
	{
	public:
		/**
		 * @brief	Constructor del boton
		 * @param	upState		Imagen para cuando el boton esta en un estado no pulsado.
		 * @param	downState	Imagen para cuando el boton esta pulsado
		 * @param	text		Texto que aparece en el boton
		 */
		SButton(const Shared_Ptr< STexture > &upState, const Shared_Ptr< STexture > &downState, const char *text = "", const char *font_name = "font.gxfont")
		{
			upTexture		= upState;
			downTexture		= downState;
			mainImage.reset (new SImage(upTexture));
			enabled			= true;
			isPressed		= false;
			scaleWhenDown	= downState != upState ? Scale : (Scale * 0.9f);

			callBack = NULL;

			addChild(mainImage);
		
			width  = upTexture->texture->GetWidth() > downTexture->texture->GetWidth() ? upTexture->texture->GetWidth() : downTexture->texture->GetWidth();
			height = upTexture->texture->GetHeight() > downTexture->texture->GetHeight() ? upTexture->texture->GetHeight() : downTexture->texture->GetHeight();

			width = width * Scale;
			height = height * Scale;
			
			if(text != "")
			{
				this->text.reset(new SText(text, font_name, 0xff000000, CIwFVec2(width, height)));
				addChild(this->text);
			}
		}

		~SButton(){};

	public:
		void Update	();
		void Release();
		void setVisible(bool show);

	private:
		void ResetContents		();
		void OnTouch			();

	private:
		Shared_Ptr< STexture >	upTexture;
		Shared_Ptr< STexture >  downTexture;
		Shared_Ptr< SText >		text;
		Shared_Ptr< SImage >	mainImage;

	public:
		float	scaleWhenDown;
		bool	enabled;
		bool	isPressed;

	public:
		typedef void(*CallBack)();
		CallBack callBack;
		void SetCallback(CallBack cb)	{ callBack = cb;};

		void SetColor	(const uint &color)			{ text->SetColor(color); };
		void SetText	(const char *string)		{ text->SetText(string); };

	};
	
}

#endif