#ifndef _S_INPUT_
#define _S_INPUT_

#include <s3ePointer.h>
#include "SUpdate.hpp"

namespace storm
{
	/**
	 * @brief Maneja los eventos de toque.
	 * Sigue el patron de dise�o Singleton para poder ser instanciado
	 * una sola vez.
	 */
	class SInput
	{
	public:
		float	touch_x, touch_y;	// Posiciones X e Y de donde se ha hecho un toque
		bool	isTouched;			// Estado de tocado
		bool	prevTouched;		// Si ha sido tocado antes

	public:
		static SInput *GetInstance();

		void Reset ();
		
		static void TouchScreenCB(s3ePointerEvent *event);
		static void TouchScreenMoveCB(s3ePointerMotionEvent *event);
	
		~SInput(){};
	private:

		SInput();
		
		static SInput *inputSingleton;

	};
}

#endif