#ifndef _DISPLAY_HEADER
#define _DISPLAY_HEADER

    #include <s3eSurface.h>

    namespace storm
    {

        /**
         * @brief La clase Display representa a la pantalla del dispositivo.
         */
        class SDisplay
        {
        public:

            enum Orientation
            {
                PORTRAIT,
                LANDSCAPE
            };

        private:

            static int  dWidth;
            static int	dHeight;

        public:

			static void Initialize(int32, int32);

            static Orientation orientation ()
            {
                return (width <= height ? PORTRAIT : LANDSCAPE);
            }

            static void lock_orientation (Orientation orientation, bool allow_flip = false)
            {
                ::s3eSurfaceSetInt
                (
                    S3E_SURFACE_DEVICE_ORIENTATION_LOCK,
                    orientation == PORTRAIT ? (allow_flip ? S3E_SURFACE_PORTRAIT : S3E_SURFACE_PORTRAIT_FIXED ) : (allow_flip ? S3E_SURFACE_LANDSCAPE : S3E_SURFACE_LANDSCAPE_FIXED )
                );
            }

            /// Retorna el ancho de la pantalla en pixels.
            /// Puede variar para un mismo dispositivo seg�n la pantalla est� vertical o apaisada.
            static int  width ()
            {
                return (dWidth);
            }

            /// Retorna el alto de la pantalla en pixels.
            /// Puede variar para un mismo dispositivo seg�n la pantalla est� vertical o apaisada.
            static int  height ()
            {
                return (dHeight);
            }

            static int  quantized_width ()
            {
                return (quantize (dWidth));
            }

            static int  quantized_height ()
            {
                return (quantize (dHeight));
            }

        private:

            static int   quantize (int resolution);

        };

    }

#endif