#ifndef _STWEEN_MANAGER_
#define _STWEEN_MANAGER_

#include "SUpdate.hpp"
#include "SDisplayObject.hpp"
#include "vector"

using storm::SUpdate;
using toolkit::Shared_Ptr;
using std::vector;
using storm::SDisplayObject;

namespace storm
{
	class STweenManager : public SUpdate
	{
	public:
		static STweenManager *GetInstance();

		void Update();
		void addTween(Shared_Ptr< SDisplayObject> object, float time, float posX, float posY);

		~STweenManager();

	private:
		STweenManager();
		static STweenManager *inputSingleton;
		vector< Shared_Ptr< SDisplayObject> > *v_objects;
		vector< float > *v_times;
		vector< float > *v_posx;
		vector< float > *v_posy;

		int num_tweens;
	};
}

#endif