#ifndef _SIMAGE_
#define _SIMAGE_

#include "SDisplayObject.hpp"
#include "STexture.hpp"
#include "Shared_Ptr.hpp"

using toolkit::Shared_Ptr;
using storm::SDisplayObject;

namespace storm
{
	class SImage : public SDisplayObject
	{

	public:
		Shared_Ptr< STexture > Texture;

	public:
		SImage(){};
		SImage(const Shared_Ptr< STexture > &texture);
		~SImage();

		void Draw		();
		void Initialize	();
		void Release	();

		virtual void Update(){};

		void SetTexture(const Shared_Ptr< STexture > &texture);
		
	};
}

#endif