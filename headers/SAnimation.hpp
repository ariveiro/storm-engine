#ifndef _S_ANIMATION_
#define _S_ANIMATION_

#include <Iw2D.h>
#include <string>
#include "SImage.hpp"
#include "SSprite.hpp"
#include <iostream>
#include "StormEngine.hpp"

using std::string;
using storm::SImage;
using storm::SSprite;
using toolkit::Shared_Ptr;

namespace storm
{
	/**
	 * @brief Sprite animado
	 */
	class SAnimation
	{
	public:
		SAnimation(string name, int anim_time, int frames, bool loop = false); //le pasamos la duraci�n en milisegundos
		~SAnimation();

	public:
		void addTexture(const Shared_Ptr <STexture> &entrada);
		Shared_Ptr< STexture > getFrame(int pos){return atlas->at(pos);};
		float getLimitCount(){return limit_count;};
		int	getFrames(){return frames;};
		string getName(){return name;};

	private: //funcs

	private:
		Shared_Ptr <vector <Shared_Ptr <STexture> > > atlas;
		int		counter;
		float	limit_count;
		int		frames;
		int		currentFrameNumber;
		string	name;

	public:
		bool	loop;
		float	fps;
		int		anim_time;

	};

	
}

#endif
