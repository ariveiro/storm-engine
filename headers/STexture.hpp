#ifndef _S_TEXTURE_
#define _S_TEXTURE_

#include "Iw2D.h"
#include "Shared_Ptr.hpp"

using toolkit::Shared_Ptr;

namespace storm
{
	class STexture
	{
	public:
		
		STexture(const char *file_url, bool from_res = false)
		{
			if(!from_res)
				texture.reset(Iw2DCreateImage(file_url));
			else
				texture.reset(Iw2DCreateImageResource(file_url));
		}

		~STexture(){}
		
	public:

		Shared_Ptr< CIw2DImage > texture;
		
	};
}

#endif