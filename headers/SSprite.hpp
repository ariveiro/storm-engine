#ifndef _SSPRITE_
#define _SSPRITE_

#include <s3e.h>
#include <Iw2D.h>
#include "SImage.hpp"
#include <std\c++\vector>
#include "SDisplayObject.hpp"
#include "Shared_Ptr.hpp"

using storm::SDisplayObject;
using std::vector;
using toolkit::Shared_Ptr;

namespace storm
{
	
	/**
	 * @brief Funciona como un contenedor de cualquier tipo de SDisplayObect,
	 * incluido esta misma clase, SSprite.
	 */
	class SSprite: public SDisplayObject
	{
	public:

		SSprite();
		~SSprite();

	protected:

		Shared_Ptr< vector< Shared_Ptr< SDisplayObject > > > Children;  // Vector que contiene todas las im�genes hijas de un SSprite

	public:

		virtual void Draw	(); // Recorre la lista de hijos y llama a su funci�n Draw
		void Initialize		();
		virtual void Update	();
		virtual void Release(); // Recorre la lista de hijos eliminando uno a uno
		void UpdatePos		();
		void setPivot		(float x, float y);

		void addChild		(const Shared_Ptr< SDisplayObject > &child);
		void removeChild	(const Shared_Ptr< SDisplayObject > &child);

		virtual void setScale   (float scale);
		virtual void setPosition(float x, float y);
		virtual void setRotation(float angle);
		virtual void setVisible (bool show);

	};
}

#endif