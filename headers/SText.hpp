#ifndef _S_TEXT_
#define _S_TEXT_

#include <Iw2D.h>
#include <string>
#include "SDisplayObject.hpp"
#include "Shared_Ptr.hpp"


using std::string;
using toolkit::Shared_Ptr;

namespace storm
{
	class SText : public SDisplayObject
	{
	public:
		SText(const char *text, const char *font, const uint32 &color, const CIwFVec2 &size);
		~SText(){};

		virtual void Update		(){};
		virtual void Draw		();
		virtual void Release	();

		void SetFont	(const char *font);
		void SetColor	(const uint &color);
		void SetText	(const char *string);
		const char* GetText();

		float getWidth() { return bounds.x; };
		float getHeight(){ return bounds.y; };

	private:
		Shared_Ptr< CIw2DFont >	font;
		uint32		color;
		const char  *text;
		CIwFVec2	bounds;
	};
}

#endif