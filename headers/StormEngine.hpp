#ifndef _STORM_ENGINE_
#define _STORM_ENGINE_

#include "IwGx.h"
#include "Iw2D.h"
#include "SUpdate.hpp"
#include "SSprite.hpp"
#include "s3ePointer.h"
#include "s3e.h"
#include "IwDebug.h"
#include "SInput.hpp"
#include <iostream>
#include "s3eTimer.h"
#include "SDisplay.hpp"
#include "Shared_Ptr.hpp"

namespace storm
{
	class StormEngine : public SUpdate
	{
	private:

		float	delay;
		float	sLastFrameTimestamp;
		float	now;
		float	passedTime;

		static float deltaTime;
		static bool	 sStarted;

		SSprite *mainContainer;

	public:

		StormEngine ( int32 width, int32 height, float _fps );
		~StormEngine();

	public:
		
		void	SetContainer	( SSprite *container );
		void	Start			();
		void	Update			();

		static void		SetFPS	(float fps) { deltaTime = 1000 / (fps < 1 ? 1 : fps > 60 ? 60 : fps); };
		static float	GetFPS	() { return (1000 / deltaTime); };

		static int32_t	Resume	(void * system_data, void * user_data);
		static int32_t	Stop	(void * system_data, void * user_data);
	};
}

#endif