#ifndef _S_ANIMSPRITE_
#define _S_ANIMSPRITE_

#include "SAnimation.hpp"

using std::string;
using storm::SImage;
using storm::SSprite;
using toolkit::Shared_Ptr;

namespace storm
{
	/**
	 * @brief Sprite animado
	 */
	class SAnimSprite : public SImage
	{
	public:
		SAnimSprite(){};
		SAnimSprite(Shared_Ptr< STexture> texture);
		~SAnimSprite(){};

	public:
		void Update	();
		void Release();
		
		void addAnim(const Shared_Ptr <SAnimation>&animation);
		void addIdle(const Shared_Ptr <SAnimation>&animation, float wait);

		void resetIdleCount(){idle_count = 0;};	//este m�todo se utiliza cuando se realiza una acci�n que no tiene una animaci�n concreta

		virtual void stop();
		virtual void play(int i = 0);

		bool isPlaying(){return active;};
		bool isPlayingIdle(){return b_active_idle;};

	private: //funcs

	private:
		Shared_Ptr <vector <Shared_Ptr <SAnimation> > > animations;
		Shared_Ptr <SAnimation> idle;
		Shared_Ptr< STexture > tex_default;
		
		bool	b_idle;
		bool	b_active_idle;
		bool	first;
		int		counter;
		float	limit_count;
		int		frames;
		int		currentAnimation;
		int		currentFrameNumber;
		int		anim_time;

		int		idle_count;
		float	idle_wait; //tiempo de espera para que salte el idle

		bool	active;
		bool	loop;

		int		fps;	

	};
}

#endif
