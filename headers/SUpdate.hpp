#ifndef _I_UPDATE_
#define _I_UPDATE_

namespace storm
{

    /**
	 * @brief Base para cualquier clase que necesite la propiedad de ser actualizable.
     * <br><br>
    */
    class SUpdate
    {
    public:

        virtual ~SUpdate(){}

    public:

        virtual void Update () = 0;

    };
}

#endif