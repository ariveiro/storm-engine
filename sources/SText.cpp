#include "SText.hpp"

namespace storm
{
	SText::SText(const char *text, const char *font, const uint32 &color, const CIwFVec2 &size)
	{
		this->font.reset(Iw2DCreateFontResource(font));
		this->color  = color;
		this->bounds = size;
		this->text	 = text;
	}

	void SText::SetFont(const char *font)
	{
		this->font.reset(Iw2DCreateFont(font));
	}

	void SText::SetColor(const uint &color)
	{
		this->color = color;
	}

	void SText::SetText(const char *string)
	{
		text = string;
	}

	const char* SText::GetText()
	{
		return text;
	}

	void SText::Draw()
	{
		if (!text || !Visible)
			return;

		if(needRedraw)
			BuildTransformationMatrix();

		Iw2DSetTransformMatrix(TransformMatrix);
		Iw2DSetFont(font.get());
		Iw2DSetColour(color);
		Iw2DDrawString(text, Position, bounds, IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_CENTRE); 
		Iw2DSetColour(0xffffffff);
		Iw2DSetTransformMatrix(CIwFMat2D::g_Identity);
	}

	void SText::Release()
	{
		font.reset();
	}
}