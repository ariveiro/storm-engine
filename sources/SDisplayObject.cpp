#include <SDisplayObject.hpp>
#include "iostream"

using std::cout;
using std::endl;

namespace storm
{
	SDisplayObject::SDisplayObject()
	{	
		parent = NULL;
		initialized = false;
		Initialize();
	}

	SDisplayObject::~SDisplayObject(){}

	/**
	 * Inicializa el objeto poniendo todas sus propiedades
	 * a su valor inicial y construyendo la matriz de transforamcion.
	 */
	void SDisplayObject::Initialize()
	{
		IwTrace(STORM_ENGINE, ("[SDisplayObject] Initialized"));

		TransformMatrix.SetIdentity();
        TransformMatrix.m[0][0] = IW_GEOM_ONE;
        TransformMatrix.m[1][1] = IW_GEOM_ONE;

		Position.x = 0.0f;
		Position.y = 0.0f;

		Rotation = 0.0f;
		Scale	 = 1.0f;

		pivotX = 0.0f;
		pivotY = 0.0f;

		Visible		= true;
		needRedraw	= false;
		initialized = true;

		BuildTransformationMatrix();
	}

	/**
	 * Construye la matriz de transformacion, la cual se encarga
	 * de controlar la rotacion, la escala y la posicion de un objeto.
	 */
	void SDisplayObject::BuildTransformationMatrix()
	{
		if(initialized)
		{
			TransformMatrix.SetRot(Rotation);
			TransformMatrix.Scale(Scale);
			TransformMatrix.SetTrans(Position);

			if(parent)
				TransformMatrix.PostMult(parent->getTransform());

			needRedraw = false;
		}
	}

	/**
	 * Comprueba si el jugador ha tocado el objeto
	 * @return	Si ha tocado el jugador
	 */
	bool SDisplayObject::CheckUserTouch()
	{	
		/*if( SInput::GetInstance()->touch_x >= getTransform().GetTrans().x &&
			SInput::GetInstance()->touch_x <= getTransform().GetTrans().x + getWidth() &&
			SInput::GetInstance()->touch_y >= getTransform().GetTrans().y &&
			SInput::GetInstance()->touch_y <= getTransform().GetTrans().y + getHeight())
			return true;
		else
			return false;*/

		if( SInput::GetInstance()->touch_x >= Position.x &&
			SInput::GetInstance()->touch_x <= Position.x + getWidth() &&
			SInput::GetInstance()->touch_y >= Position.y &&
			SInput::GetInstance()->touch_y <= Position.y + getHeight())
			return true;
		else
			return false;
	}

	void SDisplayObject::setPivot(float x, float y)
	{
		pivotX = x;
		pivotY = y;

		needRedraw = true;
	}


	/**
	 * Establece la posicion de un objeto en X e Y
	 */
	void SDisplayObject::setPosition(float x, float y)
	{
		Position.x = x;
		Position.y = y;

		needRedraw = true;
	}

	/**
	 * Modifica la posicion en X de un objeto
	 */
	void SDisplayObject::setX(float x)
	{
		Position.x = x;
		needRedraw = true;
	}

	/*
	 * Modifica la posicion en Y de un objeto
	 */
	void SDisplayObject::setY(float y)
	{
		Position.y = y;
		needRedraw = true;
	}

	/**
	 * Establece la rotacion del objeto
	 */
	void SDisplayObject::setRotation(float angle)
	{
		Rotation	= angle;
		needRedraw  = true;
	}

	/**
	 * Establece la escala del objeto
	 */
	void SDisplayObject::setScale(float scale)
	{
		Scale		= scale;
		needRedraw  = true;
	}
	
	bool SDisplayObject::collides(float pos_x, float pos_y, float ewidth, float eheight)
	{
		if((Position.x + width) < pos_x || (Position.y + height) < pos_y ||
			Position.x > (pos_x + ewidth) || Position.y > (pos_y + eheight))
			return false;

		return true;
	}
}