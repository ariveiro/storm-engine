#include "STweenManager.hpp"

namespace storm
{
	STweenManager *STweenManager::inputSingleton = NULL;

	STweenManager *STweenManager::GetInstance()
	{
		if (inputSingleton == NULL)
			inputSingleton = new STweenManager();
		
		return inputSingleton;
	}

	STweenManager::STweenManager()
	{
		v_objects = new vector< Shared_Ptr< SDisplayObject > >();
		v_times	  = new vector<float>();
		v_posx	  = new vector<float>();
		v_posy	  = new vector<float>();

		num_tweens = 0;
	}

	void STweenManager::addTween(Shared_Ptr< SDisplayObject> object, float time, float posX, float posY)
	{
		v_objects->push_back(object);
		v_times->push_back(time);
		v_posx->push_back(posX);
		v_posy->push_back(posY);
		num_tweens++;
	}

	void STweenManager::Update()
	{
		for (int i = 0; i < num_tweens; i++)
		{
			v_objects->at(i)->setX((v_objects->at(i)->getX() + (v_posx->at(i) * v_times->at(i))));
			v_objects->at(i)->setY((v_objects->at(i)->getY() + (v_posy->at(i) * v_times->at(i))));

			if (v_objects->at(i)->getX() >= v_posx->at(i) && v_objects->at(i)->getY() >= v_posy->at(i))
			{
				v_objects->at(i)->setX(v_posx->at(i));
				//v_objects->at(i)->setY(v_posy->at(i));

				num_tweens--;
				v_objects->erase(v_objects->begin()+i);
			}
		}
	}

	STweenManager::~STweenManager()
	{
		delete v_objects;
		delete v_times;
		delete v_posx;
		delete v_posy;
	}
}