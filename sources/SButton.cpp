#include "SButton.hpp"

namespace storm
{
	
	void SButton::Update()
	{
		if (enabled)
		{
			if (SInput::GetInstance()->isTouched && !SInput::GetInstance()->prevTouched)
				OnTouch();
			else if (!SInput::GetInstance()->isTouched && isPressed)
				ResetContents();
		}
		else
		{
			if(isPressed)
				ResetContents();
		}
	}

	
	void SButton::ResetContents()
	{
		isPressed = false;
		mainImage->SetTexture(upTexture);
		setScale(1);
	}

	void SButton::OnTouch()
	{
		if(CheckUserTouch())
		{
			if (!isPressed)
			{
				mainImage->SetTexture(downTexture);
				setScale(scaleWhenDown);
				isPressed = true;
				
				if(callBack != NULL)
					callBack();
			}
		}
	}

	void SButton::setVisible(bool show)
	{
		SSprite::setVisible(show);
		enabled = show;
	}

	void SButton::Release()
	{
		mainImage.reset();
		upTexture.reset();
		downTexture.reset();
		text.reset();
	}
}