#include <SInput.hpp>
#include "iostream"

namespace storm
{

	SInput *SInput::inputSingleton = NULL;

	SInput::SInput() : isTouched(false), prevTouched(false)
	{
		s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)TouchScreenCB, 0);
		s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)TouchScreenMoveCB, 0);
	}

	SInput *SInput::GetInstance()
	{
		if (inputSingleton == NULL)
			inputSingleton = new SInput();
		
		return inputSingleton;
	}

	/**
	 * @brief Callback lanzado cuando el jugador toca la pantalla
	 */
	void SInput::TouchScreenCB(s3ePointerEvent *event)
	{
		inputSingleton->prevTouched = inputSingleton->isTouched;
		inputSingleton->isTouched = event->m_Pressed ? true : false;
		inputSingleton->touch_x = (float)event->m_x;
		inputSingleton->touch_y = (float)event->m_y;
	}

	/**
	 * @brief Callback lanzado cuando el jugador toca y mueve el dedo por la pantalla
	 */
	void SInput::TouchScreenMoveCB(s3ePointerMotionEvent *event)
	{
		inputSingleton->touch_x = (float)event->m_x;
		inputSingleton->touch_y = (float)event->m_y;
	}

	/**
	 * @brief Resetea el estado del toque
     */
	void SInput::Reset()
	{
		if (isTouched==false)
		{
			isTouched	= false;
			prevTouched = false;
		}
		else
		{
			isTouched	= true;
			prevTouched = false;
		}
		
	}
}