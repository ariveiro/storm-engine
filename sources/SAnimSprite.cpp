#include "SAnimSprite.hpp"

namespace storm
{

	SAnimSprite::SAnimSprite(Shared_Ptr< STexture > texture)
	{
		b_active_idle = false;
		b_idle = false;
		tex_default = texture;
		active = false;
		animations.reset(new vector <Shared_Ptr <SAnimation> >);
		
		SetTexture(tex_default);

		fps = 60;
		counter = 0;
		first = true;
	}

	void SAnimSprite::Update()
	{
		if (active)
		{
			if(b_active_idle)
			{
				if(counter > limit_count)
				{
					SetTexture(idle->getFrame(currentFrameNumber));
					counter = 0;
				
					if (currentFrameNumber >= (frames -1)) //toca el �ltimo frame de la animaci�n
					{
						currentFrameNumber = 0;
						if (!loop)
							active = false;
					}
					else
						currentFrameNumber++;
				}
				else
					counter++;
			}
			else
			{
				idle_count = 0;

				if(counter > limit_count)
				{
					SetTexture(animations->at(currentAnimation)->getFrame(currentFrameNumber));
					counter = 0;
				
					if (currentFrameNumber >= (frames -1)) //toca el �ltimo frame de la animaci�n
					{
						currentFrameNumber = 0;
						if (!loop)
							active = false;
					}
					else
						currentFrameNumber++;
				}
				else
					counter++;
			}
		}
		else if(b_idle)
		{
			idle_count++;
		
			if(idle_count > StormEngine::GetFPS()* idle_wait)
			{
				b_active_idle = true;
				idle_count = 0;
				active = true;
				limit_count = idle->getLimitCount();
				frames = idle->getFrames();
				currentFrameNumber = 0;	
			}
		}

	}

	void SAnimSprite::addAnim(const Shared_Ptr <SAnimation> &animation)
	{
		if(!animations)
			animations.reset(new vector <Shared_Ptr <SAnimation> >);

		animations->push_back(animation);
	}

	void SAnimSprite::addIdle(const Shared_Ptr <SAnimation>&animation, float wait)
	{
		idle_wait = wait;
		idle_count = 0;
		b_idle = true;
		idle=(animation);
	}

	void SAnimSprite::play(int i)
	{
		if(i != currentAnimation)
		{
			b_active_idle = false;
			currentAnimation = i;
			active = true;
			limit_count = animations->at(currentAnimation)->getLimitCount();
			frames = animations->at(currentAnimation)->getFrames();
			currentFrameNumber = 0;	
		}
	}

	void SAnimSprite::stop()
	{
		active = false;
		SetTexture(tex_default);
	}


	void SAnimSprite::Release()
	{
		animations->clear();
		animations.reset();
	}
}
