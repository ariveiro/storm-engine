#include "StormEngine.hpp"

using namespace storm;

namespace storm
{

	float StormEngine::deltaTime = 0;
	bool StormEngine::sStarted = false;
	/*
	 * @brief Inicializa el motor y comienza el loop principal.
	 * @param container	SSprite con el que se iniciar� el motor.
	 * @param fps		Imagenes por segundo
	 */
	StormEngine::StormEngine(int32 width, int32 height, float _fps)
	{
		Iw2DInit();
		IwGxInit();

		Iw2DSetUseMipMapping(false);

		s3eDeviceRegister	(S3E_DEVICE_PAUSE, Stop, NULL);
		s3eDeviceRegister	(S3E_DEVICE_UNPAUSE, Stop, NULL);
		s3eSurfaceSetup		(S3E_SURFACE_PIXEL_TYPE_RGB888, 0, NULL, S3E_SURFACE_BLIT_DIR_NATIVE);

		SDisplay::Initialize(Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight());

		deltaTime = 1000 / (_fps < 1 ? 1 : _fps > 60 ? 60 : _fps);

		IwTrace(STORM_ENGINE, ("Storm Engine Started"));
	}

	StormEngine::~StormEngine()
	{
	}

	void StormEngine::SetContainer(SSprite *container)
	{
		mainContainer = container;
	}

	/**
	 * @brief Necesario para que el motor comience a dibujar en pantalla
	 * y a actualizarse
	 */
	void StormEngine::Start()
	{
		sStarted = true;	
		Update();
	}

	/**
	 * @brief Reanuda el motor despu�s de que el usuario haya vuelta a la
	 * aplicacion
	 */
	int32_t StormEngine::Resume(void *system_data, void *user_data)
	{
		sStarted = true;	
		return 0;
	}

	/*
	 * @brief El motor sigue corriendo pero deja de llamar
	 * a Update y Draw del mainContainer
	 */
	int32_t StormEngine::Stop(void *system_data, void *user_data)
	{
		sStarted = false;
		return 0;
	}


	/*
	 *	@brief El loop principal del motor.
	 *	Se actualizan todos los eventos y se renderizan todas las
	 *	im�genes.
	 */
	void StormEngine::Update()
	{
		// Loop forever, until the user or the OS performs some action to quit the app
		while (!s3eDeviceCheckQuitRequest())
		{
			IwGxPrintFrameRate(0,0);
			now	= float(s3eTimerGetUST()) / 1000.0f;

			if(mainContainer)
			{

				//Mantiene la luz del telefono encendida aunque el usuario no interactue
				s3eDeviceBacklightOn();

				//Limpia la pantalla
				Iw2DSurfaceClear(0);

				//Update the input systems
				s3eKeyboardUpdate();
				if (s3eKeyboardGetState(s3eKeyAbsBSK) & S3E_KEY_STATE_DOWN)    // Back key is used to exit on some platforms
					break;

				//Actualizacion del sistema de toque
				s3ePointerUpdate();

				// Actualizacion del SSprite principal si el rendering esta activo
				if (sStarted)
				{
					mainContainer->Update();
					mainContainer->Draw();
					SInput::GetInstance()->Reset();
				}

				// Show the surface
				Iw2DFinishDrawing();
				Iw2DSurfaceShow	 ();

				passedTime	= s3eTimerGetUST() / 1000.0f;
				delay		= deltaTime - (passedTime - now);

				if(delay < 0) delay = 0;

				s3eDeviceYield(delay);

			}
			else
				break;
		}

		
		mainContainer->Release();
		delete mainContainer;

		delete SInput::GetInstance();

		// Shut down Marmalade graphics system and the Iw2D module
		Iw2DTerminate();
		IwGxTerminate();
	}
}