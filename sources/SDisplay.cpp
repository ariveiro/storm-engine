#include <Iw2D.h>
#include <s3eDevice.h>
#include <SDisplay.hpp>

namespace storm
{

	int SDisplay::dWidth  = 0;
	int SDisplay::dHeight = 0;

	void SDisplay::Initialize(int32 _width, int32 _height)
	{
		dWidth  = _width;
		dHeight = _height;
	}

    int SDisplay::quantize (int size)
    {
        static const int quantized_sizes[] =
        {
             240,  320,  400,  480,  540,  600,  640,  720,  768,  800,  854,  960,
            1024, 1080, 1136, 1152, 1200, 1280, 1440, 1536, 1600, 1920,
            2048, 2160, 2560, 3840, 4320, 7680, 8192
        };

        if (size < quantized_sizes[sizeof(quantized_sizes) / sizeof(int) - 1])
        {
            for (const int * quantized_size = quantized_sizes; ; quantized_size++)
            {
                if (size <= *quantized_size)
                {
                    return (*quantized_size);
                }
            }
        }

        return (size);
    }

}
