#include "SAnimation.hpp"

namespace storm
{

	SAnimation::SAnimation(string name, int anim_time, int frames, bool loop) //duraci�n anim en milisegundos!
	{
		this->frames = frames;
		this->anim_time = anim_time;
		atlas.reset(new vector <Shared_Ptr <STexture> >);
		this->loop = loop;
		fps = StormEngine::GetFPS();
		counter = 0;
		limit_count = fps * this->anim_time / (1000 * this->frames);
		currentFrameNumber = 0;
	}

	void SAnimation::addTexture(const Shared_Ptr <STexture>&entrada)
	{
		atlas->push_back(entrada);
	}

	SAnimation::~SAnimation()
	{
		atlas->clear();
		atlas.reset();
	}
}
