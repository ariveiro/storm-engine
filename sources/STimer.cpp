#include "STimer.hpp"
#include "iostream"

namespace storm
{
	STimer::STimer() 
	{
		resetted = true;
		running = false;

		beg			= 0;
		end			= 0;
		passedTime	= 0;
	}

	void STimer::setTimer(unsigned long milliseconds, CallBack cb)
	{
		std::cout << "Milisegundos: " << milliseconds << std::endl;

		end = milliseconds;
		callBack = cb;
	}

	void STimer::start() 
	{
		if(!running) 
		{
			if(resetted)
				beg = (unsigned long) clock();
			else
				beg -= end - (unsigned long) clock();

			std::cout << "Start Time: " << beg << std::endl;

			running = true;
			resetted = false;
		}
	}


	void STimer::stop()
	{
		if(running) 
		{
			end = (unsigned long) clock();
			running = false;
		}
	}

	void STimer::Update()
	{
		if (running)
		{
			passedTime = (unsigned long) clock() - beg;

			std::cout << "Tiempo pasado: " << passedTime << std::endl;
			std::cout << "Tiempo Final: "  << end << std::endl;

			if(passedTime >= end)
			{
				reset();
				callBack();
			}
		}
	}

	void STimer::reset() 
	{
		bool wereRunning = running;

		if(wereRunning)
			stop();

		resetted = true;
		beg = 0;
		end = 0;
	}

	unsigned long STimer::getTime()
	{
		if(running)
			return ((unsigned long) clock() - beg);
		else
			return end - beg;
	}


	bool STimer::isOver(unsigned long seconds)
	{
		return seconds >= getTime();
	}
}