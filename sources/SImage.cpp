#include <SImage.hpp>

namespace storm
{
	SImage::SImage(const Shared_Ptr< STexture > &texture)
	{
		SImage::Initialize();

		Texture = texture;

		width  = texture->texture->GetWidth()  * Scale;
		height = texture->texture->GetHeight() * Scale;
	}

	SImage::~SImage(){}

	void SImage::Initialize()
	{
		SDisplayObject::Initialize();
	}

	/**
	 * Establece la textura que se mostrara
	 */
	void SImage::SetTexture(const Shared_Ptr< STexture > &texture)
	{
		Texture = texture;

		width  = texture->texture->GetWidth()  * Scale;
		height = texture->texture->GetHeight() * Scale;
	}

	void SImage::Draw()
	{
		if (!Texture || !Visible)
			return;

		if(needRedraw)
			BuildTransformationMatrix();

		Iw2DSetTransformMatrix(TransformMatrix);
		Iw2DDrawImage(Texture->texture.get(), CIwFVec2(pivotX, pivotY));
		Iw2DSetTransformMatrix(CIwFMat2D::g_Identity);
	}

	void SImage::Release()
	{
		Texture.reset();
		parent = NULL;
	}

}