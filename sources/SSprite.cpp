#include <SSprite.hpp>
#include <iostream>

using namespace std;

namespace storm
{
	SSprite::SSprite()
	{
		SSprite::Initialize();
	}

	SSprite::~SSprite()
	{
	}

	void SSprite::Initialize()
	{
		SDisplayObject::Initialize();
		Children.reset(new vector< Shared_Ptr< SDisplayObject > >);
	}

	void SSprite::Update()
	{
		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->Update();
		}
	}

	void SSprite::UpdatePos()
	{
		SDisplayObject::UpdatePos();
		
		uint length = Children->size();		
		for (uint i = 0; i < length; i++)
		{
			Children->at(i)->UpdatePos();
		}
	}

	/*
	 * A�ade una imagen a la lista de hijos
	 */
	void SSprite::addChild(const Shared_Ptr< SDisplayObject > &child)
	{
		Children->push_back(child);
		child->SetParent(this);
	}

	/*
	 * Elimina un hijo concreto de la lista de hijos
	 */
	void SSprite::removeChild(const Shared_Ptr< SDisplayObject > &child)
	{
		uint length = Children->size();
		bool found = false;

		for (uint i = 0; i < length; ++i)
		{
			if (Children->at(i) == child)
			{
				Children->at(i).reset();
				Children->erase(Children->begin()+i);
				found = true;
				break;
			}
		}

		if(found)
			IwTrace(STORM_ENGINE, ("El hijo que ha sido eliminado correctamente."));
		else
			IwTrace(STORM_ENGINE, ("El hijo que ha intentando eliminar no existe o ya fue eliminado."));
	}

	void SSprite::setPosition(float x, float y)
	{
		SDisplayObject::setPosition(x, y);

		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->UpdatePos();
		}	
	}

	void SSprite::setRotation(float angle)
	{
		SDisplayObject::setRotation(angle);

		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->setRotation(Children->at(i)->getRotation() + angle);
		}
	}

	void SSprite::setScale(float scale)
	{
		SDisplayObject::setScale(scale);
		
		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->setScale(scale);
		}
	}

	void SSprite::setVisible(bool show)
	{
		SDisplayObject::setVisible(show);

		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->setVisible(show);
		}
	}

	void SSprite::setPivot(float x, float y)
	{
		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->setPivot(x, y);
		}
	}

	void SSprite::Draw()
	{
		if (!Visible)
			return;

		if(needRedraw)
			BuildTransformationMatrix();

		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->Draw();
		}
	}

	void SSprite::Release()
	{
		uint length = Children->size();
		for (uint i = 0; i < length; ++i)
		{
			Children->at(i)->Release();
			Children->at(i).reset();
		}

		Children->clear();
		Children.reset();

		parent = NULL;
	}
}